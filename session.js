module.exports = {
	options : {
		storage    : 'memory',
		session_id : 'pill-sid',
		id_length  : 512,
		max_age    : 10*60*1000 // 10 minutes
	},

	init : function(options) {
		var storage = options.storage;
		if (typeof storage === 'string') {
			if ( ! this.storages.hasOwnProperty(storage)) {
				throw new Error('Storage "' + storage + '" not found');
			}
			storage = new this.storages[storage](options);
		}

		this.app.use(function(req, res, next) {

			// Find session by id
			var id = req.cookie[options.session_id];

			if ( ! id) {
				id = this.generateId();
			}

			// Touch session id
			res.cookie(options.session_id, id, {
				maxAge : options.max_age,
				path   : '/'
			});

			req.session = {};

			res.on('finish', function(err) {
				if (err) {
					console.error(err);
				}

				storage.set(id, req.session, options.max_age, function (err) {
					if (err) console.log(err);
				});
			});

			storage.get(id, function(err, session) {

				req.session = session;
				next();
			});

		}.bind(this));
	},

	S4 : function() {
	   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	},

	generateId : function () {
		var guid = '';
		while (guid.length < this.options.id_length) {
			guid += this.S4();
		}
		return guid;
	},

	storages : {
		memory : MemoryStorage
	}
};

/**
 * Memory storage
 * @param {Object} options Options: max age, connection params, etc
 * @constructor
 */
function MemoryStorage(options) {
	this.storage = {};
	this.maxAge  = options.max_age;
}

/**
 * Save session to storage
 * @param  {String}   id       Session id
 * @param  {Object}   session  Session object
 * @param  {Number}   expires  Expiration date
 * @param  {Function} callback Resul callback
 */
MemoryStorage.prototype.set = function(id, session, expires, callback) {
	if (typeof expires === 'function' || ! expires) {
		callback = expires;
		expires  = Date.now() + this.maxAge;
	}
	this.storage[id] = { data : session, expires : expires };
	if (callback) callback(null, this.storage[id].data);
};

/**
 * Get session object
 * @param  {String}   id       Session id
 * @param  {Function} callback Result callback
 */
MemoryStorage.prototype.get = function(id, callback) {
	var session;
	session = this.storage[id];
	if ( ! session) {
		this.set(id, {}, callback);
	} else {
		callback(null, session.data);
	}
};

